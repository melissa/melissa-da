# Copyright 2020 Institut National de Recherche en Informatique et en Automatique (https://www.inria.fr/)
#                Poznan Supercomputing and Networking Center (https://www.psnc.pl/)

ARG release=20.04

FROM ubuntu:$release

# Make tzdata configure non-interactive
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ >/etc/timezone &&\
    # install Melissa DA dependencies
    apt-get update && apt-get --yes install \
        build-essential \
        gcc \
        cmake \
        gettext \
        gfortran \
        git \
        libhdf5-openmpi-dev \
        libopenblas-dev \
        libopenmpi-dev \
        libpython3-all-dev \
        python3 \
        python3-pip \
        patchelf \
        pkgconf \
        sudo \
        vim \
        zip

# patchelf is for spack
# Do not run testcases as root as mpirun does not like this...
ARG userid=1000
RUN adduser \
	--uid=$userid \
	--shell=/bin/bash \
	--gecos='Docker,,,,' \
	--disabled-password \
	docker && \
    echo "docker:docker" | chpasswd && \
# adds to sudo group. REM: vim, sudo adn adding docker to the sudo group is for
# debugging only.
    usermod -aG sudo docker

USER docker

WORKDIR /home/docker

# OpenMPI workarounds
# Fix OMPI Vader: https://github.com/open-mpi/ompi/issues/4948
ENV OMPI_MCA_btl_vader_single_copy_mechanism=none\
# Allow oversubscribing
    OMPI_MCA_rmaps_base_oversubscribe=1\
# Disable busy waiting
    OMPI_MCA_mpi_yield_when_idle=1

COPY $PWD/spack /home/docker/spack

#SHELL ["/bin/bash", "-c"]

RUN cd spack &&\
    git checkout add-melissa-da-build2 &&\
    echo ". $HOME/spack/share/spack/setup-env.sh" >> $HOME/.profile &&\
    . $HOME/spack/share/spack/setup-env.sh &&\
    spack external find &&\
    spack compiler find &&\
echo '  openblas:\n\
    externals:\n\
    - spec: openblas@0.3.8\n\
      prefix: /usr\n\
  all:\n\
    providers:\n\
      blas: [openblas]\n\
      lapack: [openblas]' >> /home/docker/.spack/packages.yaml

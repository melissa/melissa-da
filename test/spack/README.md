# Test the Spack Build

As mentioned in [README.md](../../README.md), a spack build is available for Melissa-DA.
In the following we explain how to test the spackbuild within a podman contianer.
Each call to podman should be interchangeable to a call to docker.

First you must clone the melissa spack repository:
```bash
git clone -b add-melissa-da-build2 --single-branch https://gitlab.inria.fr/melissa/spack.git
```
next we build a container to work in this will look for the spack folder in the
current working directory as retrieved by the previous command. The build takes several
minutes.
```bash
podman build -t melissa-da-spack-runner .
```
Last but not least we run the spack install plus test suite. The spack install command
will install all dependencies for melissa-da. This can take a large amount of time (in
the order of 25 minutes). Next melissa is compiled and the test suite is run which can
take about 15 minutes again -- so go get you a coffee after typing this:
```bash
podman run --rm melissa-da-spack-runner bash -l -c 'spack install --keep-stage --test=root melissa-da'
```

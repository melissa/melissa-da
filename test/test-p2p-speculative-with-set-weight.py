import os
import shutil
import configparser
import time

os.system('killall gdb')

from melissa_da_study import *
clean_old_stats()


run_melissa_da_study(
    is_p2p=True,
    runner_cmd='python3 ' + os.getenv('MELISSA_DA_SOURCE_PATH') + '/examples/simulation4-p2p/simulation_with_set_weight.py',
    total_steps=3,
    ensemble_size=15,
    procs_runner=2,
    n_runners=3,
    show_server_log=False,
    show_simulation_log=False,
    runner_timeout=10,
    server_timeout=60 * 60,
    additional_env={
        'PYTHONPATH': os.getenv('MELISSA_DA_SOURCE_PATH') + '/examples/simulation4-p2p:' + os.getenv('PYTHONPATH'),
        'SIMULATION_RANDOM_PROPAGATION_TIME': '1'
        },
    assimilator_type=ASSIMILATOR_PYTHON,
    is_speculative=True
)

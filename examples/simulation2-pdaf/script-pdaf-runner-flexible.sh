#!/bin/bash

killall xterm

# Necessary environment variables
export MELISSA_DA_PDAF_RUNNER="TRUE"
export PDAF_FILTER_NAME="ESTKF"
export MELISSA_DA_PDAF_MODEL_TASKS=3

# Set variables that normally are set by the launcher:
export MELISSA_DA_TOTAL_STEPS=18
export MELISSA_DA_ENSEMBLE_SIZE=9
#export MELISSA_DA_RUNNER_ID=0
export MELISSA_TIMING_NULL=$(($(date +%s%N)/1000000))  # time in ms

cores=$((MELISSA_DA_PDAF_MODEL_TASKS*2))

rm -rf STATS
mkdir STATS
cd STATS

DEBUG=false
if $DEBUG;
then
    mpirun --oversubscribe\
        -n $cores \
        -x MELISSA_DA_PDAF_RUNNER \
        -x MELISSA_DA_PDAF_MODEL_TASKS \
        -x PDAF_FILTER_NAME \
        -x MELISSA_DA_TOTAL_STEPS \
        -x MELISSA_DA_ENSEMBLE_SIZE \
        -x MELISSA_TIMING_NULL \
        xterm_gdb simulation2-pdaf

else
    mpirun --oversubscribe \
        -n $cores \
        -x MELISSA_DA_PDAF_RUNNER \
        -x MELISSA_DA_PDAF_MODEL_TASKS \
        -x PDAF_FILTER_NAME \
        -x MELISSA_DA_TOTAL_STEPS \
        -x MELISSA_DA_ENSEMBLE_SIZE \
        -x MELISSA_TIMING_NULL \
        simulation2-pdaf
fi

# runner written in python that calls the melissa da api
from melissa_da_api import MelissaDaApi

from mpi4py import MPI


# Main:
import numpy as np
import time

melissa = MelissaDaApi()

# IO Melissa init - only necessary for MelissaP2P
print('Old  comm size:', MPI.COMM_WORLD.size)
comm = melissa.comm_init(MPI.COMM_WORLD)
print('New  comm size:', comm.size)

#model init
state = np.zeros(2000, dtype='float64')
state[0] = 41

# Traditioinal melissa innit:
melissa.init(len(state), 0, comm)



nsteps = 1
while nsteps > 0:
    comm.Barrier()
    # do some thing with the states
    time.sleep(0.1)
    nsteps = melissa.expose(state)

    print('Now propagating %d steps' % nsteps)

print("Finished")



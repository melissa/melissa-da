/*
 * CopyAssimilator.h
 *
 *  Created on: Jan 20, 2022
 *      Author: friese
 */

#ifndef COPYASSIMILATOR_H_
#define COPYASSIMILATOR_H_

#include "Assimilator.h"


class CopyAssimilator : public Assimilator
{
private:
    Field & field;
    const int total_steps;
    MpiManager & mpi;
public:
    CopyAssimilator(Field & field_, const int total_steps, MpiManager & mpi_);
    virtual int do_update_step(const int current_step);

    virtual void on_init_state(const int runner_id, const
                               Part & part, const
                               VEC_T * values, const
                               Part & hidden_part,
                               const VEC_T * values_hidden);
};

#endif /* COPYASSIMILATOR_H_ */

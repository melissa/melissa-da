/*
 * Assimilator.h
 *
 *  Created on: Aug 14, 2019
 *      Author: friese
 */

#ifndef ASSIMILATOR_H_
#define ASSIMILATOR_H_

#include <memory>
#include "Field.h"
#include "MpiManager.h"
#include "utils.h"

// FIXME: move all assimilators into folder

/**
 * This Enum must contain one entry per assimilator.
 * There is also the list in launcher/utils.py that need to be kept up to date.
 */
enum AssimilatorType
{
    ASSIMILATOR_DUMMY = 0,
    ASSIMILATOR_PDAF = 1,
    ASSIMILATOR_EMPTY = 2,
    ASSIMILATOR_CHECK_STATELESS = 3,
    ASSIMILATOR_PRINT_INDEX_MAP = 4,
    ASSIMILATOR_WRF = 5,
    ASSIMILATOR_PYTHON = 6,
    ASSIMILATOR_COPY = 7
};

/**
 * Each Assimilator type inherites this interface class.
 * A function to perform the update step (do_update_step) must be implemented by each
 * Assimilator. This function implements how the analysis state is generated from the
 * background state.
 * Further must the constructor init the ensemble (EnsembleMember::states_analysis in
 * Field).
 *
 */
class Assimilator
{
protected:
    int nsteps = -1;
public:
// TODO: at the moment we do not need those things, but later it might be good to use them to diminish coupling....
// void put_part(const Part & part, const VEC_T * data);
// VEC_T * get_part(const Part & part) = 0;


    /**
     * This function is the heart of each Assimilator.
     * it must:
     * - set the nsteps variable
     * - change the Field ensemble_members EnsembleMember::state_analysis vector
     *   according to the EnsembleMember::state_background vector for each member.
     * - return the nsteps
     *
     * Note that this function is called on each server rank. Not the full global state
     * vectors are available. Use, e.g., [index_maps](@ref indexmaps) to know which part
     * of the domain is handled.
     *
     * @remarks An easy way to experiment with the Assimilator API is the
     * PythonAssimilator.
     *
     * @returns how many steps must be performed by the model in the next iteration.
     *          Returns -1 if it wants to quit.
     * @param[in] current_step contains the current assimilation cycle number as counted
     *            by Melissa-DA
     */
    virtual int do_update_step(const int current_step) = 0;

    /**
     * This function is executed when first state messages are received from the
     * runners. Normally such messages are discarded and a proper analysis state is
     * sent back. See the CheckStatelessAssimilator to get an idea how to use this
     * function.
     * @see CheckStatelessAssimilator
     */
    virtual void on_init_state(const int, const Part &, const
                               VEC_T *, const Part &,
                               const VEC_T * )
    {
    };


    int getNSteps() const {
        return nsteps;
    };

    virtual ~Assimilator() = default;

    static std::shared_ptr<Assimilator> create(AssimilatorType assimilator_type,
                                               Field & field, const int
                                               total_steps,
                                               MpiManager & _mpi);
};




#endif /* ASSIMILATOR_H_ */

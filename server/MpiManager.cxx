#include "MpiManager.h"
#include <cassert>
#include "utils.h"
#include <csignal>
#include <execinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void
myfunc3(void)
{
    int j, nptrs;
#define SIZE 100
    void *buffer[100];
    char **strings;

    nptrs = backtrace(buffer, SIZE);
    printf("backtrace() returned %d addresses\n", nptrs);

    /* The call backtrace_symbols_fd(buffer, nptrs, STDOUT_FILENO)
        would produce similar output to the following: */

    strings = backtrace_symbols(buffer, nptrs);
    if (strings == NULL)
    {
        perror("backtrace_symbols");
        exit(EXIT_FAILURE);
    }

    for (j = 0; j < nptrs; j++)
        printf("%s\n", strings[j]);

    free(strings);
}

MpiManager::MpiManager() :
    m_comm_key("mpi_comm_world")
{
    m_comms[m_comm_key] = MPI_COMM_NULL;
}

void ccg_mpi_error_handler(MPI_Comm *communicator, int *error_code, ...) {
    char error_string[MPI_MAX_ERROR_STRING];
    int error_string_length;
    printf( "ccg_mpi_error_handler: entry\n");
    printf( "ccg_mpi_error_handler: error_code = %d\n", *error_code);
    MPI_Error_string(*error_code, error_string, &error_string_length);
    error_string[error_string_length] = '\0';
    printf( "ccg_mpi_error_handler: error_string = %s\n", error_string);
    /* If the MPI binding you are using supports additional information
       process the varargs here to retrieve it.
     */

    /* If you want to unwind to a different place in the call stack use
       setjmp/longjmp here to manipulate the return.
     */

    myfunc3();


    printf( "ccg_mpi_error_handler: exit\n");
    raise(SIGSEGV);
    exit( 1);
}

void MpiManager::init()
{

    MPI_Status status;
    MPI_Errhandler errhandler;
#if defined(WITH_FTI) && defined(WITH_FTI_THREADS)
    printf("Call MPI init with threads\n");
    int provided;
    MPI_Init_thread(NULL, NULL, MPI_THREAD_MULTIPLE, &provided);
    if( provided < MPI_THREAD_MULTIPLE )
    {
        D( "thread level is not provided!" );
        MPI_Abort( MPI_COMM_WORLD, -1 );
    }
#else
    printf("Call MPI init\n");
    MPI_Init(NULL, NULL);
    MPI_Comm_create_errhandler(&ccg_mpi_error_handler, &errhandler);
    MPI_Comm_set_errhandler(MPI_COMM_WORLD, errhandler);
#endif
    m_comms[m_comm_key] = MPI_COMM_WORLD;
    MPI_Comm_size( m_comms[m_comm_key], &m_size );
    MPI_Comm_rank( m_comms[m_comm_key], &m_rank );
}

void MpiManager::register_comm( std::string key, MPI_Comm & comm )
{
    m_comms[key] = std::move(comm);
}

void MpiManager::set_comm( std::string key )
{
    assert( m_comms.count(key) > 0 && "invalid key for MPI communicator!");

    m_comm_key = key;

    MPI_Comm_size( m_comms[m_comm_key], &m_size );
    MPI_Comm_rank( m_comms[m_comm_key], &m_rank );
}

const MPI_Comm & MpiManager::comm()
{
    // return MPI_COMM_WORLD;
    return m_comms[m_comm_key];
}

const int & MpiManager::size()
{
    return m_size;
}

const int & MpiManager::rank()
{
    return m_rank;
}

void MpiManager::finalize()
{
    MPI_Finalize();
}


MPI_Fint MpiManager::fortranComm()
{
    return MPI_Comm_c2f(comm());
}

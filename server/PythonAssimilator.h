/*
 * PythonAssimilator.h
 *
 *  Created on: Jan 14, 2021
 *      Author: friese
 */

#ifndef PYTHONASSIMILATOR_H_
#define PYTHONASSIMILATOR_H_

#include "Assimilator.h"

#define PY_SSIZE_T_CLEAN
#include <Python.h>

namespace py
{
extern PyObject *pFunc;
extern PyObject *pModule;
extern wchar_t *program;
extern PyObject *pEnsemble_list_background;
extern PyObject *pEnsemble_list_analysis;
extern PyObject *pEnsemble_list_hidden_inout;

extern PyObject *pArray_assimilated_index;
extern PyObject *pArray_assimilated_varid;

void init(Field &field);
int callback(const int current_step);
void finalize();
void err(bool no_fail, const char * error_str);
}

/**
 * This Assimilator will call a python function with the following signature
 * to perform the state update every assimilation cycle:
 * ```python
 * def callback(cycle, ensemble_list_background, ensemble_list_analysis,
 *         ensemble_list_hidden_inout, assimilated_index, assimilated_varid):
 * ```
 * That is searched in the python file from the `MELISSA_DA_PYTHON_ASSIMILATOR_MODULE`
 * environment variable. This allows to implement assimilator modules python that can
 * see the ensemble of background states and manipulate the ensemble of analysis states.
 *
 * An example can be found for example in:
 * examples/simulation1/script_assimilate_python.py
 */
class PythonAssimilator : public Assimilator
{
private:
    Field & field;
    const int total_steps;
    MpiManager & mpi;
public:
    PythonAssimilator(Field & field_, const int total_steps, MpiManager & mpi_);

    virtual void on_init_state(const int runner_id, const
                               Part & part, const
                               VEC_T * values, const
                               Part & hidden_part,
                               const VEC_T * values_hidden);

    virtual int do_update_step(const int current_step);
    virtual ~PythonAssimilator();
};

#endif /* PYTHONASSIMILATOR_H_ */

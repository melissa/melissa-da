/******************************************************************
*                            Melissa                              *
*-----------------------------------------------------------------*
*   COPYRIGHT (C) 2017  by INRIA and EDF. ALL RIGHTS RESERVED.    *
*                                                                 *
* This source is covered by the BSD 3-Clause License.             *
* Refer to the  LICENSE file for further information.             *
*                                                                 *
*-----------------------------------------------------------------*
*  Original Contributors:                                         *
*    Theophile Terraz,                                            *
*    Bruno Raffin,                                                *
*    Alejandro Ribes,                                             *
*    Bertrand Iooss,                                              *
******************************************************************/

#include "melissa_messages.h"
#include "melissa_utils.h"

#include <stdio.h>
#include <string.h>
#include <zmq.h>

#ifdef DEBUG_MELISSA_MESSAGES
int msend(zmq_msg_t *msg, void *b, void *c, int line, char* file, char* func) {
    printf("sending message from line %d, file %s, function %s\n", line, file, func);
    printf("message_id: %d\n", *((int*)zmq_msg_data(msg)));
    return zmq_msg_send(msg,b,c);
}
#define zmq_msg_send(a,b,c) msend(a,b,c, __LINE__, __FILE__, __func__)
#endif

int get_message_type(char* buff)
{
//    int* buff_ptr = buff;
    int msg_type = *(int*)buff;
//    return *buff_ptr;
    return msg_type;
}

void message_hello (zmq_msg_t *msg)
{
    char* buff_ptr = NULL;
    zmq_msg_init_size (msg, sizeof(int));
    buff_ptr = (char*)zmq_msg_data (msg);
    *((int*)buff_ptr) = (int)HELLO;
}

int send_message_hello (void* socket,
                        int   flags)
{
    zmq_msg_t msg;
    message_hello (&msg);
    return zmq_msg_send (&msg, socket, flags);
}

void message_stop (zmq_msg_t *msg)
{
    char* buff_ptr = NULL;
    zmq_msg_init_size (msg, sizeof(int));
    buff_ptr = zmq_msg_data (msg);
    *((int*)buff_ptr) = (int)STOP;
}

int send_message_stop(void* socket,
                      int   flags)
{
    zmq_msg_t msg;
    message_stop (&msg);
    return zmq_msg_send (&msg, socket, flags);
}

void message_simu_status (zmq_msg_t *msg,
                          int simu_id,
                          int status)
{
    int*      buff_ptr = NULL;
    zmq_msg_init_size (msg, 3 * sizeof(int));
    buff_ptr = (int*)zmq_msg_data (msg);
    *buff_ptr = (int)SIMU_STATUS;
    buff_ptr ++;
    *buff_ptr = simu_id;
    buff_ptr ++;
    *buff_ptr = status;
}

int send_message_simu_status (int   simu_id,
                              int   status,
                              void* socket,
                              int   flags)
{
    zmq_msg_t msg;
    message_simu_status (&msg,
                         simu_id,
                         status);
    return zmq_msg_send (&msg, socket, flags);
}

void message_server_name (zmq_msg_t *msg,
                          char* node_name,
                          int   rank)
{
    char*     buff_ptr = NULL;
    zmq_msg_init_size (msg, 2 * sizeof(int) + strlen(node_name) +1);
    buff_ptr = zmq_msg_data (msg);
    *((int*)buff_ptr) = (int)SERVER;
    buff_ptr += sizeof(int);
    *((int*)buff_ptr) = rank;
    buff_ptr += sizeof(int);
    strcpy (buff_ptr, node_name);
}

int send_message_server_name (char* node_name,
                              int   rank,
                              void* socket,
                              int   flags)
{
    zmq_msg_t msg;
    message_server_name (&msg,
                         node_name,
                         rank);
    return zmq_msg_send (&msg, socket, flags);
}

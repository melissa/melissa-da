/******************************************************************
*                            Melissa                              *
*-----------------------------------------------------------------*
*   COPYRIGHT (C) 2017  by INRIA and EDF. ALL RIGHTS RESERVED.    *
*                                                                 *
* This source is covered by the BSD 3-Clause License.             *
* Refer to the  LICENSE file for further information.             *
*                                                                 *
*-----------------------------------------------------------------*
*  Original Contributors:                                         *
*    Theophile Terraz,                                            *
*    Bruno Raffin,                                                *
*    Alejandro Ribes,                                             *
*    Bertrand Iooss,                                              *
******************************************************************/

#ifndef MELISSA_MESSAGES_H_
#define MELISSA_MESSAGES_H_

#include <zmq.h>

#ifdef __cplusplus
extern "C" {
#endif

#define HELLO 10
#define STOP 3
#define SIMU_STATUS 5
#define SERVER 6
#define ALIVE 7

// SimulationStatus as defined in the launcher:
enum SimulationStatus
{
    NOT_SUBMITTED = -1,
    PENDING = 0,
    WAITING = 0,
    RUNNING = 1,
    FINISHED = 2,
    TIMEOUT = 4
};

int get_message_type(char* buff);

void message_hello(zmq_msg_t *msg);

int send_message_hello (void* socket,
                        int flags);

void message_stop (zmq_msg_t *msg);

int send_message_stop (void* socket,
                       int flags);

void message_simu_status (zmq_msg_t *msg,
                          int simu_id,
                          int status);

int send_message_simu_status (int simu_id,
                              int status,
                              void* socket,
                              int flags);

void message_server_name(zmq_msg_t *msg,
                         char* node_name,
                         int rank);

int send_message_server_name (char* node_name,
                              int rank,
                              void* socket,
                              int flags);

void message_simu_data (zmq_msg_t *msg,
                        int time_stamp,
                        int simu_id,
                        int client_rank,
                        int vect_size,
                        int nb_vect,
                        const char* field_name,
                        double** data_ptr);

int send_message_simu_data(int time_stamp,
                           int simu_id,
                           int client_rank,
                           int vect_size,
                           int nb_vect,
                           const char* field_name,
                           double** data_ptr,
                           void*    socket,
                           int flags);

#ifdef __cplusplus
}
#endif

#endif /* MELISSA_MESSAGES_H_ */

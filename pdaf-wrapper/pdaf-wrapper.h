/*
 *
 * pdaf.h
 *
 *  Created on: Aug 15, 2019
 *      Author: friese
 */

#ifndef PDAF_WRAPPER_H_
#define PDAF_WRAPPER_H_
#include <stdio.h>
#ifdef __cplusplus
extern "C" {
#endif


#include "../common/melissa_da_stype.h"

// TODO: choose better names!
// TODO: pass parameters by value better?

void cwrapper_init_pdaf(
    const int * param_dim_state_p,
    const int * param_total_steps,
    const int * dim_index_map,
    const INDEX_MAP_T param_index_map[],
    const int * dim_index_map_hidden,
    const INDEX_MAP_T param_index_map_hidden[]);

void cwrapper_assimilate_pdaf();
void cwrapper_PDAF_deallocate();

// old, TODO: remove, also remove from f90 file.
int cwrapper_PDAF_get_state(int * doexit, const int * dim_state_analysis,
                            double state_analysis[], int * status);
void cwrapper_PDAF_put_state(const int * dim_state_background, const
                             double state_background[], int * status);

void cwrapper_set_current_step(const int * new_current_step);

// as arrays of pointers do not work in fortran, call this function once per ensemble
// member_id so the function needs to handle it intelligently to not always open and
// close the netcdf file (check member_id against dim_ens or 0 to know if it should open
// or not ;)
void cwrapper_init_ens_hidden(const int * dim_p, const int * dim_ens, const
                              int * member_id, double state_p[]);


void cwrapper_init_parallel_pdaf(const int * ensemble_size,
                                 const int * in_comm_world,
                                 int * out_comm_world,
                                 int * out_task_id);


void cwrapper_init_parallel(const int * comm);



double cwrapper_get_time_from_c();

void cwrapper_get_member_id_from_pdaf(int * out_member_id);

void cwrapper_get_model_task_id(int * out_model_task_id);

#ifdef __cplusplus
}
#endif


#endif /* PDAF_WRAPPER_H_ */

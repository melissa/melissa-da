#include "pdaf-wrapper.h"
#include <chrono>
#include <cstdlib>
#include "../common/utils.h"

double cwrapper_get_time_from_c() {
    // this functionality could also be achieved calling stufff rom the common lib using the timing object in the api/server but this would lead to ring dependencies...
    static std::chrono::high_resolution_clock::time_point null_time(std::chrono::milliseconds(atoll(getenv(
                                                                                                        "MELISSA_TIMING_NULL"))));
    TimePoint now = std::chrono::high_resolution_clock::now();
    return std::chrono::duration<double, std::milli>(now - null_time).count();
}

#include "melissa_da_api.h"
#include "ApiTiming.h"
#include "Part.h"
#include "ZeroMQ.h"
#include "melissa_da_config.h"
#include "melissa_utils.h"
#include "messages.h"
#include "utils.h"

#include <cassert>
#include <csignal>
#include <cstdlib>
#include <cstring>

#include <map>
#include <memory>
#include <string>
#include <type_traits>

#include <mpi.h>


// TODO ensure sizeof(size_t is the same on server and api... also for other
// types?? but the asserts are doing this already at the beginning as we receive
// exactly 2 ints.... Forward declarations:
#ifdef WITH_P2P
#include "p2p/app_core.h"
#endif

#include "api_common.h"

#include "pdaf_runner.h"

void melissa_finalize();
struct ConfigurationConnection;

// Global Variables:
bool no_mpi = false;
void* context = nullptr;
int last_step = -2;
/// Communicator used for simulation/Runner
Server server;
std::map<int, std::unique_ptr<ServerRankConnection> > ServerRanks::ranks;
Field field;
ConfigurationConnection* ccon = nullptr;


#ifdef REPORT_TIMING
std::unique_ptr<ApiTiming> timing(nullptr);
#endif


// TODO: kill if no server response for a timeout...

struct ConfigurationConnection
{
    void* socket;
    ConfigurationConnection() {
        socket = zmq_socket(context, ZMQ_REQ);
        char* melissa_server_master_node = getenv("MELISSA_SERVER_MASTER_NODE");
        if(melissa_server_master_node == nullptr)
        {
            L("You must set the MELISSA_SERVER_MASTER_NODE environment "
              "variable before running!");
            assert(false);
        }
        std::string port_name = fix_port_name(melissa_server_master_node);
        D("Configuration Connection to %s", port_name.c_str());
        zmq_connect(socket, port_name.c_str());
    }

    /// returns true if field registering is requested by the server
    bool register_runner_id() {
        int header[] = {REGISTER_RUNNER_ID, melissa_get_runner_id()};

        zmq::send_n(socket, header, 2);

        L("registering runner_id %d at server", melissa_get_runner_id());

        auto msg_reply = zmq::recv(socket);

        assert(zmq::size(*msg_reply) == 2 * sizeof(int));

        int reply[2] = {0};

        std::memcpy(reply, zmq::data(*msg_reply), sizeof(reply));

        bool request_register_field = reply[0] != 0;

        L("Registering field? %d", reply[0]);

        server.comm_size = reply[1];

        size_t port_names_size =
            server.comm_size * MPI_MAX_PROCESSOR_NAME * sizeof(char);

        assert_more_zmq_messages(socket);
        msg_reply = zmq::recv(socket);

        assert(zmq::size(*msg_reply) == port_names_size);

        server.port_names.resize(zmq::size(*msg_reply));

        std::memcpy(
            server.port_names.data(),
            zmq::data(*msg_reply), zmq::size(*msg_reply));

        return request_register_field;
    }

    // TODO: high water mark and so on?
    void register_field(
        const char* field_name, size_t local_vect_sizes[],
        size_t local_hidden_vect_sizes[],
        std::vector<INDEX_MAP_T>& global_index_map,
        std::vector<INDEX_MAP_T>& global_index_map_hidden,
        const int bytes_per_element, const int bytes_per_element_hidden) {

        int type = REGISTER_FIELD;
        int header[] = {
            type, comm_size, bytes_per_element, bytes_per_element_hidden
        };
        auto msg_header = zmq::msg_init(sizeof(header) +
                                        MPI_MAX_PROCESSOR_NAME);

        std::memcpy(zmq::data(*msg_header), header, sizeof(header));
        std::strncpy(
            zmq::data(*msg_header) + sizeof(header),
            field_name,
            MPI_MAX_PROCESSOR_NAME);
        zmq::send(*msg_header, socket, ZMQ_SNDMORE);
        zmq::send_n(
            socket, local_vect_sizes, comm_size, ZMQ_SNDMORE);
        zmq::send_n(
            socket, local_hidden_vect_sizes, comm_size, ZMQ_SNDMORE);

        auto msg_index_map = zmq::msg_init_n(
            global_index_map.data(), global_index_map.size());

        zmq::send(*msg_index_map, socket, ZMQ_SNDMORE);

        auto msg_index_map_hidden = zmq::msg_init_n(
            global_index_map_hidden.data(), global_index_map_hidden.size());

        zmq::send(*msg_index_map_hidden, socket);

        auto msg_reply = zmq::recv(socket);
        (void)msg_reply;

        assert(zmq::size(*msg_reply) == 0);
    }


    ~ConfigurationConnection() {
        zmq_close(socket);
    }
};

/// returns true if needs field to be registered on the server. This can only
/// happen on comm_rank 0
bool register_runner() {
    if (is_p2p())
    {
        return false;
    }

    bool register_field = false;
    if(comm_rank == 0)
    {
        ccon = new ConfigurationConnection();
        register_field = ccon->register_runner_id();
    }

    int runner_id = melissa_get_runner_id();
    MPI_Bcast(&runner_id, 1, MPI_INT, 0, comm);

    MPI_Bcast(&server.comm_size, 1, MPI_INT, 0, comm);
    if(comm_rank != 0)
    {
        server.port_names.resize(server.comm_size * MPI_MAX_PROCESSOR_NAME);
    }
    D("port_names_size= %lu", server.port_names.size());
    MPI_Bcast(
        server.port_names.data(), server.comm_size * MPI_MAX_PROCESSOR_NAME,
        MPI_CHAR, 0, comm);

    // D("Portnames %s , %s ", server.port_names.data(),
    // server.port_names.data() + MPI_MAX_PROCESSOR_NAME);
    return register_field;
}




// TODO: later one could send the index map over all ranks in parallel!
void gather_global_index_map(
    const size_t local_vect_size, const INDEX_MAP_T local_index_map[],
    std::vector<INDEX_MAP_T>& global_index_map, const size_t local_vect_sizes[],
    const int bytes_per_element) {
    if(local_index_map == nullptr)
    {
        int i = 0;
        for(auto& e : global_index_map)
        {
            e.index = i++;
            e.varid = 0;
        }
    }
    else
    {
        size_t displs[comm_size];
        size_t last_displ = 0;
        size_t rcounts[comm_size];
        // FIXME!!!: this breaks if we have more than INTMAX array elements
        // which is the case... move to int...
        std::copy(local_vect_sizes, local_vect_sizes + comm_size, rcounts);
        for(int i = 0; i < comm_size; ++i)
        {
            rcounts[i] /= bytes_per_element;
            displs[i] = last_displ;
            last_displ += local_vect_sizes[i] / bytes_per_element;
        }


        slow_MPI_Gatherv(
            local_index_map, local_vect_size / bytes_per_element,
            MPI_MY_INDEX_MAP_T, global_index_map.data(), rcounts, displs,
            MPI_MY_INDEX_MAP_T, 0, comm);
    }
}

void melissa_init_with_index_map(
    const char* field_name, const size_t local_vect_size,
    const size_t local_hidden_vect_size, const int bytes_per_element,
    const int bytes_per_element_hidden, MPI_Comm comm_,
    const INDEX_MAP_T local_index_map[],
    const INDEX_MAP_T local_index_map_hidden[]) {
    // TODO: field_name is actually unneeded. its only used to name the output
    // files in the server side...
    //
    init_utils();

    comm = comm_;

    // activate logging:
    MPI_Comm_rank(comm, &comm_rank);

    // for convenience
    MPI_Comm_size(comm, &comm_size);

#ifdef REPORT_TIMING
    // Start Timing:
    try_init_timing();
    trigger(START_ITERATION, last_step);
#endif

    bool register_field = false;
    if (!pdaf_runner::active())
    {
        context = zmq_ctx_new();
        register_field = register_runner();
    }

    // Check That bytes_per_element are the same on all ranks
    int tmp_assert = bytes_per_element;
    MPI_Bcast(&tmp_assert, 1, MPI_INT, 0, comm);
    assert(tmp_assert == bytes_per_element);

    // Check That bytes_per_element_hidden are the same on all ranks
    tmp_assert = bytes_per_element_hidden;
    MPI_Bcast(&tmp_assert, 1, MPI_INT, 0, comm);
    assert(tmp_assert == bytes_per_element_hidden);

    // create field
    field.name = field_name;
    field.current_state_id = -1; // We are beginning like this...
    field.current_step = 0;
    field.local_vect_size = local_vect_size;
    field.local_hidden_vect_size = local_hidden_vect_size;

    if (is_p2p())
    {
#ifdef WITH_P2P
        melissa_p2p_init(field_name,
                         local_vect_size,
                         local_hidden_vect_size,
                         bytes_per_element,
                         bytes_per_element_hidden,
                         local_index_map,
                         local_index_map_hidden
                         );

        trigger(START_PROPAGATE_STATE, field.current_state_id);
        return;
#else
        L("May not start with this configuration. p2p works only with FTI");
        exit(1);
#endif
    }

    std::vector<size_t> local_vect_sizes(comm_size);
    // synchronize local_vect_sizes and
    MPI_Allgather(
        &field.local_vect_size, 1, my_MPI_SIZE_T, local_vect_sizes.data(), 1,
        my_MPI_SIZE_T, comm);

    std::vector<size_t> local_hidden_vect_sizes(comm_size);
    // synchronize local_hidden_vect_sizes and
    MPI_Allgather(
        &field.local_hidden_vect_size, 1, my_MPI_SIZE_T,
        local_hidden_vect_sizes.data(), 1, my_MPI_SIZE_T, comm);


    if(local_hidden_vect_sizes.size() >= 2)
    {
        D("Vect sizes: %lu %lu", local_vect_sizes[0], local_vect_sizes[1]);
    }


    // gather the global index map from this
    std::vector<INDEX_MAP_T> global_index_map; // TODO: use special index map
                                               // type that can encode the var
                                               // name as well!!
    std::vector<INDEX_MAP_T> global_index_map_hidden;
    if(comm_rank == 0)
    {
        size_t global_vect_size = sum_vec(local_vect_sizes);
        assert(global_vect_size % bytes_per_element == 0);
        global_index_map.resize(sum_vec(local_vect_sizes) / bytes_per_element);

        size_t global_vect_size_hidden = sum_vec(local_hidden_vect_sizes);
        assert(global_vect_size_hidden % bytes_per_element_hidden == 0);
        global_index_map_hidden.resize(
            global_vect_size_hidden / bytes_per_element_hidden);
    }

    gather_global_index_map(
        local_vect_size, local_index_map, global_index_map,
        local_vect_sizes.data(), bytes_per_element);

    gather_global_index_map(
        local_hidden_vect_size, local_index_map_hidden, global_index_map_hidden,
        local_hidden_vect_sizes.data(), bytes_per_element_hidden);

    // if (comm_rank == 0)
    // {
    // D("Global index map:");
    // print_vector(global_index_map);
    // }

    if (pdaf_runner::active())
    {
        pdaf_runner::init_with_index_map(field_name,
                                         local_vect_size,
                                         local_hidden_vect_size,
                                         bytes_per_element,
                                         bytes_per_element_hidden,
                                         comm_,
                                         local_index_map,
                                         local_index_map_hidden
                                         );
    }
    else
    {
        if(register_field)
        {
            // Tell the server which kind of data he has to expect
            ccon->register_field(
                field_name, local_vect_sizes.data(), local_hidden_vect_sizes.data(),
                global_index_map, global_index_map_hidden, bytes_per_element,
                bytes_per_element_hidden);
        }


        // Calculate to which server ports the local part of the field will connect
        field.initConnections(
            local_vect_sizes, local_hidden_vect_sizes, bytes_per_element,
            bytes_per_element_hidden);
    }

    trigger(START_PROPAGATE_STATE, field.current_state_id);
}

// can be called from fortran or if no mpi is used (set NULL as the mpi
// communicator) TODO: check if null is not already used by something else!
void melissa_init_no_mpi(
    const char* field_name, const size_t* local_doubles_count,
    const size_t* local_hidden_doubles_count) { // comm is casted into an
                                                // pointer to an mpi
                                                // communicaotr if not null.
    MPI_Init(NULL, NULL); // TODO: maybe we also do not need to do this? what
                          // happens if we clear out this line?
    no_mpi = true;
    melissa_init(
        field_name, *local_doubles_count * sizeof(double),
        *local_hidden_doubles_count * sizeof(double), sizeof(double),
        sizeof(double), MPI_COMM_WORLD);
}


int melissa_expose(
    const char* field_name, VEC_T* values, VEC_T* hidden_values) {


    print_debug_hash(values, field.local_vect_size, "Before expose");
    print_debug_hash(hidden_values, field.local_hidden_vect_size, "Before expose hidden");

    // MPI_Barrier(comm);
    trigger(STOP_PROPAGATE_STATE, field.current_state_id);
    trigger(START_IDLE_RUNNER, melissa_get_runner_id());
    assert(phase != PHASE_FINAL);
    if(phase == PHASE_INIT)
    {
        phase = PHASE_SIMULATION;
        // finalize initializations
        // First time in melissa_expose.
        // Now we are sure all fields are registered.
        // we do not need this anymore:
        delete ccon;
        ccon = nullptr;
    }

    // Now Send data to the melissa server
    assert(field.name == field_name);

    int nsteps;
    if (pdaf_runner::active())
    {
        nsteps = pdaf_runner::expose(field_name, values, hidden_values);
        // These variables are reused in the event logging so we set them to meaningful values:
        field.current_step = pdaf_runner::current_step;
        field.current_state_id = pdaf_runner::get_member_id();
    }
    else if (is_p2p())
    {
#ifdef WITH_P2P
        nsteps = melissa_p2p_expose(values, hidden_values);
#else
        L("May not start with this configuration. p2p works only with FTI");
        exit(1);
#endif
    }
    else
    {
        // default expose:

        field.putState(values, hidden_values, field_name);

        // @Kai: here we could checkpoint the values variable ! using fti. The server than could recover from such a checkpoint.

        // and request new data
        nsteps = field.getState(values, hidden_values);
    }

    // MPI_Barrier(comm);
    trigger(STOP_IDLE_RUNNER, melissa_get_runner_id());

    D("nsteps %d", nsteps);

    if(nsteps > 0)
    {
        if(last_step != field.current_step)
        {
            trigger(STOP_ITERATION, last_step);
            last_step = field.current_step;
            trigger(START_ITERATION, field.current_step);
        }
        trigger(START_PROPAGATE_STATE, field.current_state_id);
        trigger(NSTEPS, nsteps);
    }
    else if (nsteps == 0)
    {
        melissa_finalize();
    }

    // TODO: this will block other fields!



    print_debug_hash(values, field.local_vect_size, "After expose");
    print_debug_hash(hidden_values, field.local_hidden_vect_size, "After expose hidden");

    return nsteps;
}

void melissa_finalize() // TODO: when using more serverranks, wait until an end
                        // message was received from every before really
                        // ending... or actually not. as we always have only an
                        // open connection to one server rank...
{
    // sleep(3);
    MPI_Barrier(comm);
    // sleep(3);
    D("End Runner.");

    phase = PHASE_FINAL;
    // TODO: free all pointers?
    // not an api function anymore but activated if a finalization message is
    // received.
    if(ccon != nullptr)
    {
        delete ccon;
    }


    // free all connections to server ranks:
    ServerRanks::ranks.clear();

    // sleep(3);
    if (context != nullptr)
    {
        D("Terminating zmq context");
        zmq_ctx_destroy(context);
    }

#ifdef REPORT_TIMING
    if(comm_rank == 0)
    {
        timing->report(
            comm_size, field.local_vect_size + field.local_hidden_vect_size,
            melissa_get_runner_id());
    }

#ifndef REPORT_TIMING_ALL_RANKS
    if (comm_rank == 0)
#endif
    {

        // output some nice region csv to speedup post processing
        const std::array<EventTypeTranslation, 3> event_type_translations = {{
            {START_ITERATION, STOP_ITERATION, "Iteration"},
            {START_PROPAGATE_STATE, STOP_PROPAGATE_STATE, "Propagation"},
            {START_IDLE_RUNNER, STOP_IDLE_RUNNER, "Runner idle"},
        }};
        std::string fn = "melissa_runner" + std::to_string(melissa_get_runner_id());

        timing->write_region_csv(event_type_translations, fn.c_str(), comm_rank);

    }

#endif

    if(no_mpi)
    {
        // if without mpi we need to finalize mpi properly as it was only
        // created in this context.
        MPI_Finalize();
    }
}


// Helper functions to request the state of the melissa runner from outside
int melissa_get_current_step() {
    assert(phase == PHASE_SIMULATION);
    return field.current_step;
}

int melissa_is_runner() {
    char* melissa_server_master_node = getenv("MELISSA_SERVER_MASTER_NODE");
    if(melissa_server_master_node == nullptr)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

int melissa_get_current_state_id() {
    // assert(phase == PHASE_SIMULATION);
    if (pdaf_runner::active())
    {
        // return pdaf_runner::get_member_id();
        return pdaf_runner::get_model_task_id();
    }
    else
    {
        if(phase == PHASE_SIMULATION)
        {
            return field.current_state_id;
        }
        else
        {
            return -2;
        }
    }
}

int runner_id = -1;
int melissa_get_runner_id()
{

    if (runner_id == -1) {
        runner_id = atoi(getenv("MELISSA_DA_RUNNER_ID"));
    }
    return runner_id;
}

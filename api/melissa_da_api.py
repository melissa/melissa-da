import ctypes
import os

class MelissaDaApi:
    MPI_Fint = ctypes.c_int


    def __init__(self):
        self.c_lib = ctypes.CDLL(os.getenv("MELISSA_DA_PATH")+"/lib/libmelissa_da_api.so")

# void melissa_init_f(const char *field_name,
                    # const int *local_doubles_count,
                    # const int *local_hidden_doubles_count,
                    # MPI_Fint   *comm_fortran);
    def init(self, local_doubles_count, local_hidden_doubles_count, comm):
        self.field_name = "state1".encode('utf-8')
        fcomm = comm.py2f()
        self.c_lib.melissa_init_f(
                ctypes.c_char_p(self.field_name),
                ctypes.byref(ctypes.c_int(local_doubles_count)),
                ctypes.byref(ctypes.c_int(local_hidden_doubles_count)),
                ctypes.byref(MelissaDaApi.MPI_Fint(fcomm))
                )

    def comm_init(self, comm):
        self.c_lib.melissa_comm_init_f.restype = MelissaDaApi.MPI_Fint
        fcomm = comm.py2f()
        print('Python Io init called with fortran comm', fcomm)
        res = self.c_lib.melissa_comm_init_f(ctypes.byref(MelissaDaApi.MPI_Fint(fcomm)))
        print('Python Io init returned fortran comm', res)
        return comm.f2py(res)

    def expose(self, values, hidden=None):
        h = None
        if hidden is not None:
            h = hidden.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        return self.c_lib.melissa_expose_d(
            ctypes.c_char_p(self.field_name),
            values.ctypes.data_as(ctypes.POINTER(ctypes.c_double)),
            h,
                )


    def set_weight(self, weight):
        self.c_lib.melissa_set_weight(ctypes.c_double(weight))

#include "pdaf_runner.h"
#include <stdlib.h>

#include <algorithm>
#include "utils.h"
#include "api_common.h"
#include "melissa_da_stype.h"

#include "../pdaf-wrapper/pdaf-wrapper.h"


int pdaf_runner::ensemble_size;
int pdaf_runner::local_vect_size;
int pdaf_runner::local_vect_size_hidden;
bool pdaf_runner::flexible_runner;
std::map<int, VEC_T*> pdaf_runner::hidden_state_parts;
int pdaf_runner::current_step = 1;  // effectively starts at 1 as in vanlilla melissa-da...


// dirty: we set this from here...
extern int runner_id;

void pdaf_runner::init_communicators(const MPI_Comm input_comm, MPI_Comm * new_comm)
{

    mpi_set_my_error_handler();
    ensemble_size = atoi(getenv("MELISSA_DA_ENSEMBLE_SIZE"));


    // if not one modeltask per runner we are flexible ;)
    flexible_runner = atoi(getenv("MELISSA_DA_PDAF_MODEL_TASKS")) != ensemble_size;

    // PDAF_Init_Comm
    MPI_Fint comm_world_f;
    MPI_Fint input_comm_f = MPI_Comm_c2f(input_comm);

    // setup runner id to model task to have meaningful timing info!
    cwrapper_init_parallel_pdaf(&pdaf_runner::ensemble_size, &input_comm_f, &comm_world_f, &runner_id);


    MPI_Comm_dup(MPI_Comm_f2c(comm_world_f), new_comm);
}

// TODO: REFACTOR:  try to push all the pdaf accesses into an extra file? (also those coming from the server)
void pdaf_runner::init_with_index_map(const char *field_name,
                                      const size_t local_vect_size_,
                                      const size_t local_vect_size_hidden_,
                                      const int bytes_per_element,
                                      const int bytes_per_element_hidden,
                                      MPI_Comm comm_,
                                      const INDEX_MAP_T local_index_map[],
                                      const INDEX_MAP_T local_index_map_hidden[]
                                      )
{
    // gets called when in pdaf_runner mode and the runner calls melissa_da_init
    // TODO: see here if we need the global index map and other stuff that is calculated by the original init function
    //
    //
    // Since the PDAF documentation we need to call here somehow:
    // PDAF_Filter_Init
    assert(local_vect_size < INT_MAX);

    const int total_steps = atoi(getenv("MELISSA_DA_TOTAL_STEPS"));

    // transform to number doubles:
    assert(bytes_per_element == 8);  // assert is double
    assert(bytes_per_element_hidden == 8);

    pdaf_runner::local_vect_size = local_vect_size_ / 8;
    pdaf_runner::local_vect_size_hidden = local_vect_size_hidden_ / 8;

    TimePoint start = std::chrono::high_resolution_clock::now();
    cwrapper_init_pdaf(&local_vect_size, &total_steps,
                       &local_vect_size, local_index_map,
                       &local_vect_size_hidden, local_index_map_hidden);
    TimePoint now = std::chrono::high_resolution_clock::now();
    printf("[%d] PDAF init took: %f ms\n", comm_rank, diff_to_millis(now, start));;
}


void pdaf_runner::restore_hidden_state(VEC_T * hidden_values) {
    // FIXME: check this hidden state handling in a testcase! ... but not that easy since simulation2 is the only using PDAF so far and it has no hidden state...
    if (!flexible_runner)
    {
        return;
    }
    int member_id = get_member_id();

    const size_t bytes_to_copy = pdaf_runner::local_vect_size_hidden * 8;

    auto it = hidden_state_parts.find(member_id);
    if (it == hidden_state_parts.end())
    {
        L("Cannot find member_id=%d in local hidden state store! - Creating it with %lu bytes (current_step=%d)",
          member_id, bytes_to_copy, current_step);
        VEC_T * memory = reinterpret_cast<VEC_T*>(malloc(bytes_to_copy));
        it = hidden_state_parts.insert(std::pair<int, VEC_T*>(member_id, memory)).first;

        if (local_vect_size_hidden > 0)
        {
            L("Now initializing the hidden state");
            double * hidden_state_p = reinterpret_cast<double*>(memory);

            // REM: here we are calling a function that takes the array  pointer directly and
            // does not need a pointer to it...
            cwrapper_init_ens_hidden(&local_vect_size_hidden, &ensemble_size, &member_id, hidden_state_p);
        }
        if (current_step != 1)    // This may only happen at the very beginning
        {
            exit(1);
        }
    }

    memcpy(hidden_values, it->second, bytes_to_copy);
}


void pdaf_runner::store_hidden_state(VEC_T * hidden_values) {
    if (!flexible_runner)
    {
        return;
    }
    int member_id = get_member_id();
    const size_t bytes_to_copy = local_vect_size_hidden * 8;

    std::map<int, VEC_T*>::iterator it = hidden_state_parts.find(member_id);
    if (it == hidden_state_parts.end())
    {
        L("Error");
        exit(1);
    }

    memcpy(it->second, hidden_values, bytes_to_copy);
}

int pdaf_runner::expose(const char *field_name, VEC_T *values,
                        VEC_T *hidden_values)
{
    static int last_member_id = -2;
    static bool first_time = true;
    int status, doexit;
    // gets called when in pdaf_runner mode and the runner calls melissa_da_expose

    double * data = reinterpret_cast<double*>(values);  // FIXME: DIRTY CASTING

    if (first_time)    // the PDAF PAI wants us to start with a get state!
    {
        first_time = false;
        int nnsteps = cwrapper_PDAF_get_state(&doexit, &local_vect_size, data,
                                              &status);

        restore_hidden_state(hidden_values);

        assert(nnsteps > 0);
        //     ! Check whether forecast has to be performed
        if (doexit == 1 || status != 0)
        {
            // Something went wrong!
            D("PDAF get state wants us to exit? 1==%d", doexit);
            D("PDAF get state status=%d", status);

            // TODO: finish clean!
            std::raise(SIGINT);
            exit(1);
        }
        int member_id = get_member_id();
        if (last_member_id >= get_member_id())
        {
            current_step += nnsteps;
            cwrapper_set_current_step(&current_step);
        }
        last_member_id = member_id;
        return nnsteps;
    }


    cwrapper_set_current_step(&current_step);

    store_hidden_state(hidden_values);
    cwrapper_PDAF_put_state(&local_vect_size, data, &status);

    if (status != 0)
    {
        // Something went wrong!
        D("PDAF put state status=%d", status);
        // TODO: finish clean!
        std::raise(SIGINT);
        exit(1);
    }

    // get state

    int nnsteps = cwrapper_PDAF_get_state(&doexit, &local_vect_size, data,
                                          &status);
    restore_hidden_state(hidden_values);

    //     ! Check whether forecast has to be performed
    if (doexit == 1 || status != 0)
    {
        // Something went wrong!
        D("PDAF get state wants us to exit? 1==%d", doexit);
        D("PDAF get state status=%d", status);

        // TODO: finish clean!
        if (!doexit)
        {
            std::raise(SIGINT);
            exit(1);
        }
        else
        {
            nnsteps = 0;
            printf("Finished gracefully!\n");
            // model calls mpi barrier and finalize
        }
    }

    // TODO: refactor this timestepping!
    int member_id = get_member_id();
    if (last_member_id >= get_member_id())
    {
        current_step += nnsteps;
        cwrapper_set_current_step(&current_step);
    }
    last_member_id = member_id;

    return nnsteps;
}

int pdaf_runner::get_member_id()
{
    int member_id = melissa_get_runner_id();
    if (pdaf_runner::flexible_runner)
    {
        cwrapper_get_member_id_from_pdaf(&member_id);
    }
    return member_id;
}

int pdaf_runner::get_model_task_id()
{
    int model_task_id;
    cwrapper_get_model_task_id(&model_task_id);
    return model_task_id;
}


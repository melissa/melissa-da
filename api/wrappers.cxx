#include "melissa_da_api.h"
#include <mpi.h>

void melissa_init(
    const char* field_name, const size_t local_vect_size,
    const size_t local_hidden_vect_size, const int bytes_per_element,
    const int bytes_per_element_hidden, MPI_Comm comm_) {
    melissa_init_with_index_map(
        field_name, local_vect_size, local_hidden_vect_size, bytes_per_element,
        bytes_per_element_hidden, comm_, nullptr, nullptr);
}

void melissa_init_f(
    const char* field_name, const int* local_doubles_count,
    const int* local_hidden_doubles_count, MPI_Fint* comm_fortran) {

    MPI_Comm comm = MPI_Comm_f2c(*comm_fortran);
    melissa_init(
        field_name, *local_doubles_count * sizeof(double),
        *local_hidden_doubles_count * sizeof(double), sizeof(double),
        sizeof(double), comm);
}


void melissa_init_f_real(
    const char* field_name, const int* local_reals_count,
    const int* local_hidden_reals_count, MPI_Fint* comm_fortran) {

    MPI_Comm comm = MPI_Comm_f2c(*comm_fortran);
    melissa_init(
        field_name, *local_reals_count * sizeof(float),
        *local_hidden_reals_count * sizeof(float), sizeof(float),
        sizeof(float), comm);
}

int melissa_expose_f(const char* field_name, double* values) {
    return melissa_expose(field_name, reinterpret_cast<VEC_T*>(values), NULL);
}

int melissa_expose_f_real(const char* field_name, float* values) {
    return melissa_expose(field_name, reinterpret_cast<VEC_T*>(values), NULL);
}

/// legacy interface using doubles...
int melissa_expose_d(
    const char* field_name, double* values, double* hidden_values) {
    return melissa_expose(
        field_name, reinterpret_cast<VEC_T*>(values),
        reinterpret_cast<VEC_T*>(hidden_values));
}

#ifdef WITH_P2P
void melissa_set_weight_f(const float * weight) {
    melissa_set_weight(static_cast<double>(*weight));
}
#endif


#ifndef _pdaf_runner_h_
#define _pdaf_runner_h_

#include "../common/melissa_da_stype.h"
#include "mpi.h"
#include <stdlib.h>
#include <map>

namespace pdaf_runner
{

inline bool active()
{
    if (getenv("MELISSA_DA_PDAF_RUNNER"))        // FIXME: set this on a good point!
    {
        return true;
    }
    else
    {
        return false;
    }
}

void init_with_index_map(const char *field_name,
                         const size_t local_vect_size,
                         const size_t local_vect_size_hidden,
                         const int bytes_per_element,
                         const int bytes_per_element_hidden,
                         MPI_Comm comm_,
                         const INDEX_MAP_T local_index_map[],
                         const INDEX_MAP_T local_index_map_hidden[]
                         );

int expose(const char *field_name, VEC_T *values,
           VEC_T *hidden_values);

void init_communicators(const MPI_Comm input_comm, MPI_Comm * new_comm);

int get_member_id();
int get_model_task_id();

void store_hidden_state(VEC_T * hidden_values);
void restore_hidden_state(VEC_T * hidden_values);

extern int ensemble_size;
extern int local_vect_size;
extern int local_vect_size_hidden;
extern bool flexible_runner;      // True if there are multiple members per runner

extern int current_step;

extern std::map<int, VEC_T*> hidden_state_parts;
};

#endif

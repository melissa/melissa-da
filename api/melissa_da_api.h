/*
 * melissa_api.h
 *
 *  Created on: Jul 23, 2019
 *      Author: friese
 */

/**
 * @file melissa_da_api.h
 * This file contians all necessary API calls to transform a model code into a
 * Melissa-DA runner.
 *
 * Thus either the two functions (or variants)
 * - melissa_init and melissa_expose or
 * - melissa_add_chunk_ and melissa_commit_chunks_f need to be called
 *
 * If running with Particle Filters, also a call to melissa_comm_init is necessary.
 */

#ifndef API_MELISSA_API_H_
#define API_MELISSA_API_H_

#include <mpi.h>
#include "melissa_da_stype.h"

#ifdef __cplusplus
extern "C" {
#endif


// TODO: check if local_vect_size may be 0 too (it might be thinkable that some ranks have
//       no assimilated but only hidden state)
/**
 * This function initializes the connection with the Melissa DA Server
 * Each rank in the given MPI communicator must participate in
 * Melissa DA communications and call melissa_expose each iteration.
 *
 * @pre MPI_Init was called before.
 *
 * @param[in] field_name name of the field to initialize. This is obsolete. An empty
 *            string does fine. TODO: remove
 * @param[in] local_vect_size Size in bytes of the assimilated state
 * @param[in] local_hidden_vect_size Size in bytes of the hidden state. May be zero if no
 *            hidden state is used.
 * @param[in] bytes_per_element bytes per assimilated state element.
 *            Set to, e.g., sizeof(double). This ensures that the n-x-m redistributes
 *            only every bytes_per_element bytes and does not split in the middle of a
 *            datatype.
 * @param[in] bytes_per_element_hidden bytes per hidden state element.
 *            Set to, e.g., sizeof(double). This ensures that the n-x-m redistributes
 *            only every bytes_per_element_hidden bytes.
 * @param[in] comm_ MPI communicator
 */
void melissa_init(const char *field_name,
                  const size_t local_vect_size,
                  const size_t local_hidden_vect_size,
                  const int bytes_per_element,
                  const int bytes_per_element_hidden,
                  MPI_Comm comm_
                  );

/**
 * Sends values and hidden_values to the server and than copies the server's response
 * (analysis state) back in the memory segments specified by values and hidden_values.
 *
 * @pre melissa_init must be called.
 *
 * @remarks Never call melissa_expose twice without at least an MPI_Barrier in between.
 *          TODO: test what exactly happens in this case.
 *
 * @param[in] field_name Obsolete. Must be the same as specified in melissa_init
 * @param[in] values pointer to the assimilated state. The number of bytes specified
 *            as local_vect_size argument of the preceding melissa_init call will be
 *            sent to the server and overwritten by the server response.
 * @param[in] hidden_values pointer to the hidden state. The number of bytes specified
 *            as local_vect_size_hidden argument of the preceding melissa_init call will
 *            be sent to the server and overwritten by the server response. This pointer
 *            is dis regarded if no hidden state is used (melissa_init was called with
 *            local_hidden_vect_size=0).
 *
 * @returns 0 if simulation should end now. Otherwise returns nsteps, the number of
 *          timesteps that need to be simulated with the state as it was written into
 *          values and hidden_values.
 *
 * @see melissa_init
 */
int melissa_expose(const char *field_name, VEC_T *values,
                   VEC_T *hidden_values);


/**
 * Calling this function is only necessary if running with Particle Filters.
 * To permit communicator splitting for Particle Filter runs, this function must be
 * called just after MPI_Init(), but before the model calculates its data distribution
 * based on MPI_COMM_WORLD. This function will return a new communicator (new_comm) where
 * one helper core is split off per node. This new communicator shall replace the model's
 * use of MPI_COMM_WORLD.
 *
 * @pre MPI_Init was called before.
 *
 * @param[in] old_comm The old world communicator. Generally MPI_COMM_WORLD can be a good
 *            choice.
 * @param[out] new_comm The new world communicator that replaces, e.g., MPI_COMM_WORLD
 */
void melissa_comm_init(const MPI_Comm old_comm, MPI_Comm *new_comm);


/**
 * Wraps melissa_comm_init for calls from Fortran
 * @see melissa_comm_init
 * @returns new fortran communicator
 */
MPI_Fint melissa_comm_init_f(const MPI_Fint *old_comm_fortran);

/**
 * Same as melissa_init but allows to add an [index_map](@ref indexmaps).
 *
 * @param[in] local_index_map an array of annotating each *element* of the assimilated
 *            state with a varid and a place (index) identifier. Thus the length of the
 *            index map must be local_vect_size / bytes_per_element that equals the
 *            number of elements. Elements must be of type INDEX_MAP_T.
 * @param[in] local_index_map_hidden an array of annotating each *element* of the hidden
 *            state with a varid and a place (index) identifier. Thus the length of the
 *            index map must be local_hidden_vect_size / bytes_per_element_hidden that equals the
 *            number of elements. Elements must be of type INDEX_MAP_T.
 *
 * @see melissa_init, INDEX_MAP_T
 *
 * ## Index maps{#indexmaps}
 * Index maps permit to identify which chunk of data belongs to which state vector entry.
 * Since Melissa-DA supports n x m data redistribution this is crucial. Otherwise it
 * would be really hard to track which byte belongs to which part of the domain.
 *
 * A general approach is to associate a varid to each element. This encodes which
 * variable the according element in the values or hidden_values vector belongs to.
 * The index of each element is often a global index allowing to transform in x,y,z
 * coordinates.
 *
 * An index map for a very small domain could for example contain the following
 * (index, varid) tuples.
 *
 * Pressure variable (varid = 0):
 *
 * |       |       |       |
 * |-------|-------|-------|
 * | (0,0) | (1,0) | (2,0) |
 * | (3,0) | (4,0) | (5,0) |
 * | (6,0) | (7,0) | (8,0) |
 *
 * Saturation variable (varid = 1):
 * |       |       |       |
 * |-------|-------|-------|
 * | (0,1) | (1,1) | (2,1) |
 * | (3,1) | (4,1) | (5,1) |
 * | (6,1) | (7,1) | (8,1) |
 *
 * This then might get redistributed on 3 server ranks this way:
 *
 * Server Rank 0:
 * |       |       |       |       |       |       |
 * |-------|-------|-------|-------|-------|-------|
 * | (0,0) | (1,0) | (2,0) | (3,0) | (4,0) | (5,0) |
 *
 * Server Rank 1:
 * |       |       |       |       |       |       |
 * |-------|-------|-------|-------|-------|-------|
 * | (6,0) | (7,0) | (8,0) | (0,1) | (1,1) | (2,1) |
 *
 * Server Rank 2:
 * |       |       |       |       |       |       |
 * |-------|-------|-------|-------|-------|-------|
 * | (3,1) | (4,1) | (5,1) | (6,1) | (7,1) | (8,1) |
 *
 * The assimilator module will have access to these index maps together with the exposed
 * data. The index maps can now be used to retain the necessary information on the
 * structure of the exposed data to perform the state updates.
 */
void melissa_init_with_index_map(const char *field_name,
                                 const size_t local_vect_size,
                                 const size_t local_hidden_vect_size,
                                 const int bytes_per_element,
                                 const int bytes_per_element_hidden,
                                 MPI_Comm comm_,
                                 const INDEX_MAP_T local_index_map[],
                                 const INDEX_MAP_T local_index_map_hidden[]
                                 );

/**
 * When Melissa DA shall be used with simulation without MPI, melissa will implicitly
 * call MPI_Init on the single cored app.
 *
 * @param[in] local_doubles_count number of doubles to expose as assimilated state
 * @param[in] local_hidden_doubles_count number of doubles to expose as hidden state
 * see melissa_init
 */
void melissa_init_no_mpi(const char *field_name,
                         const size_t  *local_doubles_count,
                         const size_t  *local_hidden_vect_size);      // comm is casted into an pointer to an mpi communicaotr if not null.

/**
 * Legacy interface for melissa_init which assumes that only doubles are exposed
 * @param[in] local_doubles_count number of doubles to expose as assimilated state
 * @param[in] local_hidden_doubles_count number of doubles to expose as hidden state
 * @see melissa_init
 */
void melissa_init_f(const char *field_name,
                    const int *local_doubles_count,
                    const int *local_hidden_doubles_count,
                    MPI_Fint   *comm_fortran);

/**
 * Legacy interface for melissa_init which assumes that only reals are exposed
 * @param[in] local_reals_count number of doubles to expose as assimilated state
 * @param[in] local_hidden_reals_count number of doubles to expose as hidden state
 * @see melissa_init
 */
void melissa_init_f_real(const char *field_name,
                    const int *local_reals_count,
                    const int *local_hidden_reals_count,
                    MPI_Fint   *comm_fortran);


/**
 * Legacy interface to melissa expose using doubles...
 * @see melissa_expose
 */
int melissa_expose_d(const char *field_name, double *values, double *hidden_values);

/**
 * Wrapper needed for the Fortran interface when using no hidden state.
 * @remarks: Casting between Fortran and C arrays is not trivial
 * @see melissa_expose
 */
int melissa_expose_f(const char *field_name, double *values);

/**
 * Wrapper needed for the Fortran interface when using no hidden state.
 * @remarks: Casting between Fortran and C arrays is not trivial
 * @remarks: This wrapper function uses real (i.e., float) instead
 * of double precision fields
 * @see melissa_expose
 */
int melissa_expose_f_real(const char* field_name, float* values);

/**
 * Checks if the simulation was started as a Melissa-DA runner
 * @returns a value != 0 if the application linked against libmelissa_da_api was started
 *          as a Melissa-DA runner.
 */
int melissa_is_runner();

/**
 * Sometimes it is useful, e.g., to name output files, to have the current state id of the
 * state the runner is currently working on.
 *
 * @returns the state id internally used by Melissa-DA that identifies the state
 *          currently propagated on this runner. Otherwise returns -1 if no state is
 *          currently propagated. Returns -2 if before first melissa_expose or the server
 *          instructed the last melissa_expose to shut down.
 */
int melissa_get_current_state_id();




/**
 * Sometimes it is useful, e.g., to name output files, to have the runner id
 *
 * @returns the runner id internally used by Melissa-DA that identifies the runner
 */
int melissa_get_runner_id();

/**
 * Instead of setting the MELISSA_DA_PYTHON_CALCULATE_WEIGHT_MODULE when running in P2P
 * mode, one can also set the weight using this API call before calling
 * melissa_expose or melissa_commit_chunks. This permits to implement the weight
 * calculation within the solver code, making it possibly easier to access needed data
 * structures.
 * Note that this also allows to do the weight calculation on state / diagnostic variables
 * that are not part of the state that need to be stored to restart particles/members on
 * different runners.
 *
 * @param[in] weight Particle weight of the current particle that is going to be exposed.
 */
void melissa_set_weight(double weight);


/**
 * Wraps melissa_set_weight for calls from Fortran
 * @see melissa_set_weight
 *
 * @remark uses a 32bit float instead of a 64bit double
 */
void melissa_set_weight_f(const float * weight);

/**
 * Sometimes it is useful, e.g., to name output files, to have the current assimilation
 * cycle number as counted by the Melissa-DA server state the runner is currently working on.
 *
 * @returns the assimilation cycle number as internally used by Melissa-DA that
 *          identifies the assimilation cycle runners are working on
 */
int melissa_get_current_step();

/**
 * ## Chunk based API
 *
 * Sometimes the data that shall be exposed to Melissa-DA is not linear in memory. In
 * this case the chunk based API can be used. A variant of melissa_add_chunk needs to be
 * called for each chunk of interest before the simulation main loop starts.
 * Then in the main loop, only melissa_commit_chunks_f needs to be called and everything
 * is exposed. Note that melissa_commit_chunks_f automatically calls melissa_init. Thus,
 * melissa_add_chunk followed by melissa_commit_chunks_f may not be used together with
 * calls to melissa_init and melissa_expose.
 *
 * @remark There are no C-Bindings of this feature yet since no usecase existed in C yet.
 *
 * @see melissa_commit_chunks_f, melissa_add_chunk
 *
 *
 *
 * ### melissa_add_chunk_{r,i,d,l,c}
 *
 * Add a chunk of data that shall be exposed each time melissa_commit_chunks_f gets
 * called. This function mus be called only once per chunk. There are functions to add
 * different types of chunks generated by macros:
 *
 * - melissa_add_chunk_r for float type
 * - melissa_add_chunk_i for integer type
 * - melissa_add_chunk_d for double type
 * - melissa_add_chunk_l for bool/logic type
 * - melissa_add_chunk_c for char type
 *
 * @remark Note that chunks of different types can be added
 * @pre MPI_Init was called before.
 *
 * @param[in] varid specifies a user defined id the chunk belongs to.
 * @param[in] index_map The index_map annotating the values. Must contain count elements.
 * @param[in] values the content of this chunk. Must contain count elements.
 * @param[in] count the number of elements this chunk counts. This is not in bytes!
 * @param[in] is_assimilated != 0 if chunk belongs to assimilated state. Otherwise
 *            chunk belongs to the hidden state.
 *
 * @see melissa_commit_chunks_f, index_maps
 *
 * ### melissa_commit_chunks_f()
 * Performs melissa_init if necessary. After that it exposes all memory locations added
 * by preceding calls to variants of melissa_add_chunk at once.
 *
 * @remark Data at chunk adresses is sent to the server and then overwritten by the
 *         server response.
 *
 * @pre A variant of melissa_add_chunk must have been called at least once.
 *
 * @param[in] comm_fortran Fortran communicator from which each will exchange data with
 *            Melissa-DA
 *
 * @returns the same value as melissa_expose: 0 if simulation should end now. Otherwise
 *          returns nsteps, the number of timesteps that need to be simulated with the
 *          state as it was written into values and hidden_values.
 */
int melissa_commit_chunks_f(MPI_Fint * comm_fortran);

/**
 * C version of melissa_commit_chunks_f
 * @see melissa_commit_chunkgs_f
 */
int melissa_commit_chunks(MPI_Comm comm_);

#define add_chunk_wrapper_decl(TYPELETTER, CTYPE) \
    void melissa_add_chunk_ ## TYPELETTER(const int * varid, const int * index_map, \
                                          CTYPE * values, const size_t * count, \
                                          const int * is_assimilated); \
    void melissa_add_chunk_ ## TYPELETTER ## _d(const int * varid, const int * index_map, \
                                                CTYPE * values, const size_t * count, \
                                                const int * is_assimilated)

add_chunk_wrapper_decl(r, float);
add_chunk_wrapper_decl(i, int);
add_chunk_wrapper_decl(d, double);
add_chunk_wrapper_decl(l, int);
add_chunk_wrapper_decl(c, char);

#undef add_chunk_wrapper_decl




#ifdef __cplusplus
}
#endif

#endif /* API_MELISSA_API_H_ */

module purge

module load Intel/2021.4.0

# ESIAS/WRF:
#module load intel-para
module load ParaStationMPI
module load netCDF-Fortran/4.5.3

module load imkl
module load netCDF-C++4
module load HDF5
module load GDB/11.1
module load CMake/3.21.1
module load ZeroMQ

# Python Assimilator
module load Python/3.9.6
module load mpi4py/3.1.3
module load netcdf4-python/1.5.7

# for repex
module load git

ulimit -s unlimited
ulimit -c 4000000
